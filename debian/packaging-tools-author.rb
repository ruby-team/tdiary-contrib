#!/usr/bin/env ruby

#
# Extract copyright holders.
# Author:   Daigo Moriwaki <beatles@sgtpepper.net>
# Copyright (c) 2005 Daigo Moriwaki
# License:  GNU GENERAL PUBLIC LICENSE Version 2 or later.
#
$KCODE = "E"

def grep(pattern)
  lines = IO.popen("zsh -c \"grep -i copyright #{pattern}\"") {|io| io.readlines}

  authors = []
  trush   = []

  lines.each do |l|
    case l
    when /\d{4},?\s+(.*)/     # 2004(,) ....
      s = $1.strip
      s.gsub!(/by\s+/i, "")   # 2004 by ...
      authors << s
    when /by\s+(.*)$/i        # by ...
      s = $1.strip
      authors << s
    when /\(C\)\s*(.*)$/i     # (C) ...
      authors << $1.strip
    else
      trush << l
    end
  end

  authors.map! do |s|
    s.gsub!(/\(C\)\s*/,"")
    s.gsub!(/\. All Rights Reserved\./, "")
    s.gsub!(/^\d{4}-\d{4}\./,"")
    s
  end
  return authors.uniq.sort, trush
end

def output(authors, trush)
  puts authors
  puts
  puts trush
end

puts "+++ tdiary-plugin +++"
output *grep("tdiary-plugin/**/*.rb")
puts
puts "+++ tdiary-theme +++"
output *grep("tdiary-theme/**/README")
puts
puts "+++ tdiary-contrib +++"
output *grep("tdiary-contrib/**/*.rb")

