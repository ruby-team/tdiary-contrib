#!/usr/bin/env ruby

#
# Extract copyright holders.
# Author:   Daigo Moriwaki <beatles@sgtpepper.net>
# Copyright (c) 2005 Daigo Moriwaki
# License:  GNU GENERAL PUBLIC LICENSE Version 2 or later.
#
$KCODE = "E"

require 'pathname'

class Theme < Struct.new(:name, :author, :copyright, :access, :license)
  def has_license?
    license && license.length > 0
  end

  def has_author?
    author && author.length > 0
  end

  def to_s
    s = ""
    if has_license?
      s << "#{name}\n"
      s << "  " + author.strip    + "\n" if author
      s << "  " + copyright.strip + "\n" if copyright
      s << "  " + access.strip    + "\n" if access
      s << "  " + license.strip   + "\n" if license
      s << "\n"
    else
      s << "#{name}\n"
      s << "### NO LICENSE ###\n"
      s << "\n"
    end
    
    s
  end

  def valid?
    has_license?
    has_author?
  end
end


def parse_readme(f)
  theme = Theme.new
  theme.name = f.dirname.basename
  f.each_line do |line|
    case line.strip!
    when /author/i
      theme.author = line
    when /copyright/i
      theme.copyright = line
    when /access/i
      theme.access = line
    when /license/i
      theme.license = line
    end
  end
  
  theme  
end


def main
  themes = []
  errors = []
  Pathname.new('../theme').children.each do |dir|
    next unless dir.directory?
    next if /^\./ =~ dir.basename
    themes << parse_readme(dir + "README")
  end
  erros = themes.select {|theme| not theme.valid?}
  themes -= errors
 
  themes.sort! {|a,b| a.name <=> b.name}
  themes.each {|t| print t.to_s}

  puts "### ERRORS ###"
  p errors
end


main if __FILE__ == $0
